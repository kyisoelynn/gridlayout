package com.example.products;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    List<ProductData> mProductList;
    ProductData mProductData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recyclerview);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this,2);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        mProductList = new ArrayList<>();
        mProductData = new ProductData("20% off","New", R.drawable.blender, R.drawable.love, "Blender N41015","Panasonic");

        mProductList.add(mProductData);

        mProductData = new ProductData("20% off","New", R.drawable.blender2, R.drawable.love, "N41017","Smeg");

        mProductList.add(mProductData);

        mProductData = new ProductData("20% off","New", R.drawable.blender2, R.drawable.love, "Blender N41015","Panasonic");

        mProductList.add(mProductData);

        mProductData = new ProductData("20% off","New", R.drawable.blender3, R.drawable.love, "N41017","Smeg");

        mProductList.add(mProductData);

        MyAdapter myAdapter = new MyAdapter(MainActivity.this, mProductList);
        mRecyclerView.setAdapter(myAdapter);

    }
}
