package com.example.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<ProductViewHolder>{
    private Context mcontext;
    private List<ProductData> productDataList;

    public MyAdapter(Context context, List<ProductData> productDataList){
        this.mcontext = context;
        this.productDataList = productDataList;
    }


    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View productView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_view, viewGroup, false);
        return new ProductViewHolder(productView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int position) {

        productViewHolder.btnPercent.setText(productDataList.get(position).getProductPercent());
        productViewHolder.btnNow.setText(productDataList.get(position).getProductNow());
        productViewHolder.imageBlender.setImageResource(productDataList.get(position).getProductImageBlender());
        productViewHolder.imageLove.setImageResource(productDataList.get(position).getProductImageLove());
        productViewHolder.textOne.setText(productDataList.get(position).getProductTextOne());
        productViewHolder.textTwo.setText(productDataList.get(position).getProductTextTwo());



    }

    @Override
    public int getItemCount() {
        return productDataList.size();
    }
}

class ProductViewHolder extends RecyclerView.ViewHolder{
    CardView cardView;
    Button btnPercent,btnNow;
    ImageView imageBlender,imageLove;
    TextView textOne,textTwo;

    public ProductViewHolder(@NonNull View itemView) {
        super(itemView);

        btnPercent = itemView.findViewById(R.id.btn_percent);
        btnNow = itemView.findViewById(R.id.btn_now);
        imageBlender = itemView.findViewById(R.id.image_blender);
        imageLove = itemView.findViewById(R.id.image_love);
        textOne = itemView.findViewById(R.id.txt_view);
        textTwo = itemView.findViewById(R.id.txt2_view);
        cardView = itemView.findViewById(R.id.card_view);
    }
}
