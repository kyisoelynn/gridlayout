package com.example.products;

public class ProductData {
    private String productPercent;
    private String productNow;
    private int productImageBlender;
    private int productImageLove;
    private  String productTextOne;
    private String productTextTwo;

    public ProductData(String productPercent, String productNow, int productImageBlender, int productImageLove, String productTextOne, String productTextTwo) {
        this.productPercent = productPercent;
        this.productNow = productNow;
        this.productImageBlender = productImageBlender;
        this.productImageLove = productImageLove;
        this.productTextOne = productTextOne;
        this.productTextTwo = productTextTwo;
    }

    public String getProductPercent() {
        return productPercent;
    }

    public String getProductNow() {
        return productNow;
    }

    public int getProductImageBlender() {
        return productImageBlender;
    }

    public int getProductImageLove() {
        return productImageLove;
    }

    public String getProductTextOne() {
        return productTextOne;
    }

    public String getProductTextTwo() {
        return productTextTwo;
    }
}
